# FlipTop #
Dashboard widget for creating upside-down text which you can then copy to the clipboard. Source available under an MIT license. Made in DashCode. Ready to use binary available from downloads section. Tested on 10.6.8 and above, should work on systems as early as 10.4.3.

Building
--------
Editing and deploying can be done from either version of DashCode, or you can edit the files within the widget bundle with your favourite text/image editors. 

Version info
------------

Current version: 1.0.
