var origGlyphs = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789,!.?{}[]_&()⁅⁆∴∵‿⁀\"'<>";
var flipGlyphs = "ɐqɔpəɟɓɥᴉɾʞ1ɯuodbɹsʇnʌʍxʎzᗄᗺↃႧƎℲ⅁HIſ⋊ᒣWNOԀΌᖈS⊥ᑎᐱMX⅄Z0lɀƐᔭ⊆9Ł86ʻ¡˙¿}{][‾⅋)(⁆⁅∵∴⁀‿„,><";
var s = "";


function load()
{
    dashcode.setupParts();
}


function remove()
{
}


function hide()
{
}


function show()
{
calcText();
}


function sync()
{
}


function showBack(event)
{
    var front = document.getElementById("front");
    var back = document.getElementById("back");

    if (window.widget) {
        widget.prepareForTransition("ToBack");
    }

    front.style.display = "none";
    back.style.display = "block";

    if (window.widget) {
        setTimeout('widget.performTransition();', 0);
    }
}


function showFront(event)
{
    var front = document.getElementById("front");
    var back = document.getElementById("back");

    if (window.widget) {
        widget.prepareForTransition("ToFront");
    }

    front.style.display="block";
    back.style.display="none";

    if (window.widget) {
        setTimeout('widget.performTransition();', 0);
    }
}

if (window.widget) {
    widget.onremove = remove;
    widget.onhide = hide;
    widget.onshow = show;
    widget.onsync = sync;
}


function calcText(event)
{
var inputText = document.getElementById("input");
var outputText = document.getElementById("output");
text = inputText.value;

var l = text.length - 1;
var r = "";
	for (var i = l; i >= 0; --i)
	{
        var c = text.charAt(i);
        var o = origGlyphs.indexOf(c);
        if (o > -1)
        {
        r = r + flipGlyphs.charAt(o);
        }
        else 
        {
        r = r + c;
        }
    }
    s = r; // save copy in global var for clipboard actions later
	outputText.innerText = s;
}

function copyClip(event)
{
changeLabel();
widget.system("export __CF_USER_TEXT_ENCODING=0x1F5:0x8000100:0x8000100 ; /bin/echo -n '" + s + "' | /usr/bin/pbcopy", function () {}); // set text encoding first to prevent garbling of clipboard copy
}

function labelBack(event)
{
var lbl = document.getElementById("label");
lbl.innerText = "Copy to clipboard";
}

function changeLabel(event)
{
var lbl = document.getElementById("label");
lbl.innerText = "Text copied";
}
